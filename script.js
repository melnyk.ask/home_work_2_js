// 1. Які існують типи даних у Javascript?
// Примітивні типи даних: number, BigInt, string, boolean, значення null, значення undefined, symbol.
// містять лише одне значення.
// Об'єкти. Містять набори ключів і даних

// 2. У чому різниця між == і ===?
// Опреатор == при порівнянні виконує перетворення типів даних (строка перетворюється в число).
// Наприклад, порівнявши 2 == "2" отримаємо true.
// Оператор === при порівнянні не виконує перетворення типів. Тому в цьому випадку
// 2 === "2" дасть false.

// 3. Що таке оператор?
// Оператор - символ, який використовується для запису певної операції.
// Наприклад, a + b. "+" - це оператор додавання. Оператор може бути унарним (застосовується до одного операнда),
// бінарним (застосовується до двох операторів, як в наведеному прикладі).
// Математичні оператори: "+", "-", "*", "/", "%" остаток від ділення, "**" піднесення до степеня, "=", ",".
// Є інші оператори: "+=", "*=" скорочений запис, "++", "--" інкремент та декремент.
// Побітові оператори: &, |, ^, ~, <<, >>, >>>.
// Оператори порівняння: >, <, >=, <=, ==, !=, ===, !==.



let user_name = prompt("What is your name?");

while (user_name === "") { 
    user_name = prompt("What is your name?");   
}

let user_age = prompt("How old are you?");

while (
    isNaN(Number(user_age)) === true ||
    user_name === null ||
    user_name === "" ||
    user_age === null ||
    user_age === "") {
    
    if ((user_name === null && user_age === null) ||
    (user_name !== null && user_age === null)) { 
        alert("You are not allowed to visit this website");
        break;
    }
    if (user_name === null && user_age !== null) {
        user_name = prompt("What is your name?", "");
        user_age = prompt("How old are you?", user_age);
    }
    else { 
        user_name = prompt("What is your name?", user_name); 
        user_age = prompt("How old are you?", user_age);
    }
}

if (+user_age < 18 && user_name !== null) {
    alert("You are not allowed to visit this website");
}
else if (+user_age >= 18 && +user_age <= 22) {
    let answer = confirm("Are you sure you want to continue?");
    if (answer === true) {
        alert(`Welcome, ${user_name}`);
    }
    else {
        alert("You are not allowed to visit this website");
    }
}
else if (+user_age > 22) { 
    alert(`Welcome, ${user_name}`);
}

